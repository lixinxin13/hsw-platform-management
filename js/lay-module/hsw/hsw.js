layui.define(["miniMenu", "element","miniPage", "miniTheme","table"], function (exports) {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table,
            miniPage = layui.miniPage;
			
		var hsw = {
			hsw_table:function (table_html_id,api,table_cols,page){
				table.render({
				        elem: "#"+table_html_id ,
						method:'post',
				        url: api,
						headers:{"token":"BEARER "+localStorage.getItem("hsw_token")},
				        toolbar: '#toolbarDemo',
				        defaultToolbar: ['filter', 'exports', 'print', {
				            title: '提示',
				            layEvent: 'LAYTABLE_TIPS',
				            icon: 'layui-icon-tips'
				        }],
				        cols: [table_cols],
						parseData: function(res){ //res 即为原始返回的数据
						if(res.code == 200){
							return {
							  "code": res.code, //解析接口状态
							  "msg": res.msg, //解析提示文本
							  "total": res.data.total, //解析数据长度
							  "list": res.data.list //解析数据列表
							};
						}else if(res.code == 401){
							layer.msg(res.msg, {
							    time: 5000 //
							}, function () {
							    window.location.href='login.html';
							});
						}
						return {
						  "code": res.code, //解析接口状态
						  "msg": res.msg //解析提示文本
						};
						  
						  },
						  request: {
						      pageName: 'page' //页码的参数名称，默认：page
						      ,limitName: 'size' //每页数据量的参数名，默认：limit
						 },
						 response: {
						      statusName: 'code' //规定数据状态的字段名称，默认：code
						      ,statusCode: 200 //规定成功的状态码，默认：0
						      ,msgName: 'msg' //规定状态信息的字段名称，默认：msg
						      ,countName: 'total' //规定数据总数的字段名称，默认：count
						      ,dataName: 'list' //规定数据列表的字段名称，默认：data
						    } ,
				        limits: [10, 15, 20, 25, 50, 100],
				        limit: 10,
				        page: page,
				        skin: 'line'
				    });
								
					//监听表格复选框选择
					table.on('checkbox(currentTableFilter)', function (obj) {
					    console.log(obj)
					});
					
			},
			hsw_search:function(data_search_filter,currentTableId){
				// 监听搜索操作
				form.on('submit('+data_search_filter+')', function (data) {
					//执行搜索重载
					console.log(data.field);
				    table.reload(currentTableId, {
				        page: {
				            curr: 1
				        }
				        , where: data.field
				    }, 'data');
				
				    return false;
				});
			},
			hsw_table_reload:function(table_id){
				var load_index = layer.load();
				table.reload(currentTableId, {
				    page: {
				        curr: 1
				    }
				}, 'data');
			},
			/**
			 * toolbar+tool事件监听
			 */
			hsw_table_tool:function(currentTableFilter,table_html_id,curd){
				table.on('tool('+currentTableFilter+')', function (obj) {
					var data = obj.data;
				    if (obj.event === 'edit') {
				
				        var content = miniPage.getHrefContent(curd.edit.html);
				        var openWH = miniPage.getOpenWidthHeight();
				        var index = layer.open({
				            title: curd.edit.title,
				            type: 1,
				            shade: 0.2,
				            maxmin:true,
				            shadeClose: true,
				            area: [openWH[0] + 'px', openWH[1] + 'px'],
				            offset: [openWH[2] + 'px', openWH[3] + 'px'],
				            content: content,
							success:function(layero, index){
								hsw.hsw_post(curd.info.url.replace("pid",data.pid), {}, function(data) {
								
									editInit(data);
									
								});
							}
				        });
				        $(window).on("resize", function () {
				            layer.full(index);
				        });
				        return false;
				    } else if (obj.event === 'delete') {
				        layer.confirm('确定要删除么', function (index) {
							
							hsw.hsw_post(curd.del.url.replace("pid",obj.data.pid),[],function(data){
								layer.msg('操作成功，即将刷新页面', {
									time: 3000 //
								}, function() {
									hsw.hsw_html_reload(0);
								});
							});
							
				        });
				    }else if (obj.event === 'reset') {
		        layer.confirm('确定要重置密码么', function (index) {
					
					hsw.hsw_post(curd.reset.url.replace("pid",obj.data.pid),[],function(data){
												
						layer.open({
						  content: "密码重置成功,请记录：<span style='color:red'>"+data+"</span>",
						  yes: function(index, layero){
						    //do something
						    hsw.hsw_html_reload(0);
						  }
						});       
					});
					
		        });
		    }else if (obj.event === 'authorization') {
				
				var content = miniPage.getHrefContent(curd.authorization.html);
				var openWH = miniPage.getOpenWidthHeight();
				var index = layer.open({
				    title: curd.authorization.title,
				    type: 1,
				    shade: 0.2,
				    maxmin:true,
				    shadeClose: true,
				    area: [openWH[0] + 'px', openWH[1] + 'px'],
				    offset: [openWH[2] + 'px', openWH[3] + 'px'],
				    content: content,
					success:function(layero, index){
						hsw.hsw_post(curd.authorization.url.replace("pid",data.pid), {}, function(data) {
						
							authInit(data);
							
						});
						
					}
				});
				$(window).on("resize", function () {
				    layer.full(index);
				});
				return false;
		    }
				});
				table.on('toolbar('+currentTableFilter+')', function (obj) {
						
					if (obj.event === 'add') {   // 监听添加操作
						var content = miniPage.getHrefContent(curd.add.html);
						var openWH = miniPage.getOpenWidthHeight();
				
						var index = layer.open({
							title: curd.add.title,
							type: 1,
							shade: 0.2,
							maxmin:true,
							shadeClose: true,
							area: [openWH[0] + 'px', openWH[1] + 'px'],
							offset: [openWH[2] + 'px', openWH[3] + 'px'],
							content: content,
						});
						$(window).on("resize", function () {
							layer.full(index);
						});
					} else if (obj.event === 'delete') {  // 监听删除操作
						var checkStatus = table.checkStatus(table_html_id)
							, data = checkStatus.data;
							layer.confirm('确定要删除么', function (index) {
								for (var i = 0; i < data.length; i++) {
									hsw.hsw_post(curd.del.url.replace("pid",data[i].pid),[],function(data){
									});
								}
									layer.msg('操作完成，即将刷新页面', {
										time: 3000 //
									}, function() {
										hsw.hsw_html_reload(0);
									});
								
								
							});
							
					}
				});
			},
			hsw_post:function(url,todata,success){
			
				var load_index = layer.load();
				$.ajax({
					url: url,
					data:todata,
					type: 'post',
					dataType: 'json',
					beforeSend:function(xhr)
					{
					  xhr.setRequestHeader("token","BEARER "+localStorage.getItem("hsw_token"));
					},
					success:function(data){
						if(data.code == 200){
							success(data.data);
						}else if(data.code == 401){
							layer.msg(data.msg, {
							    time: 5000 //
							}, function () {
							    window.location.href='login.html';
							});
						}else{
							//这里做错误的返回
							layer.msg(data.msg, {
							    time: 5000 //
							}, function () {
							    
							});
						}
					},
					error: function (XMLHttpRequest,textStatus) {
						
					    console.log("error-----------"+textStatus);
					},
					complete:function(){
					    layer.close(load_index);  
					}

				});
			
			},
			hsw_post_json:function(url,todata,success){
			
				var load_index = layer.load();
				$.ajax({
					url: url,
					data:todata,
					type: 'post',
					dataType: 'json',
					beforeSend:function(xhr)
					{
					  xhr.setRequestHeader("token","BEARER "+localStorage.getItem("hsw_token"));
					  xhr.setRequestHeader("Content-Type","application/json");
					},
					success:function(data){
						if(data.code == 200){
							success(data.data);
						}else if(data.code == 401){
							layer.msg(data.msg, {
							    time: 5000 //
							}, function () {
							    window.location.href='login.html';
							});
						}else{
							//这里做错误的返回
							layer.msg(data.msg, {
							    time: 5000 //
							}, function () {
							    
							});
						}
					},
					error: function (XMLHttpRequest,textStatus) {
					    console.log("error-----------"+textStatus);
					},
					complete:function(){
					    layer.close(load_index);  
					}

				});
			
			},
			hsw_select:function(data,select_id){
				var opt = '';
/* 				var opt = ' <option value="">请选择</option>';
 */				for (var i in data) {
				    opt += '<option value = "' + data[i].pid + '">' + data[i].name + '</option>'
				}
				$("#"+select_id).html(opt);
			},
			hsw_html_reload:function(timer){
				setTimeout(function(){ 
					window.location.reload(); }, 
				timer);
			},
			hsw_double_select:function(select_id,name,tips,data,init){
				return xmSelect.render({
					el: '#'+select_id, 
					initValue:init,
					name: name,
					tips: tips,
					filterable: true,
					theme: {
						color: '#1cbbb4',
					},toolbar: {
						show: true,
					list: [ 'ALL', 'CLEAR', 'REVERSE' ]
					},
					data: data
				});
			},
			hsw_tree_radio_select:function(tree_id,name,treeData){
				return xmSelect.render({
			                el: '#'+tree_id, 
							name: name,
			                model: { label: { type: 'text' } },
			                radio: true,
			               clickClose: true,
			                tree: {
			                    show: true,
			                    strict: false,
			                    expandedKeys: true,
								simple: false
			                },
							toolbar: {
									show: true,
									list: ['CLEAR']
								},
			                height: '400px',
			                data(){
			                    return treeData
			                },//处理方式
							on: function(data){
								if(data.isAdd){
									return data.change.slice(0, 1)
								}
							}
			            }); 
			},
			hsw_tree_select:function(tree_id,name,treeData){
				return xmSelect.render({
			                el: '#'+tree_id, 
							autoRow: true,
							filterable: true,
							tree: {
									show: true,
									showFolderIcon: true,
									showLine: true,
									indent: 20,
									expandedKeys: true,
									simple: true
							},
							toolbar: {
								show: true,
								ist: ['ALL', 'REVERSE', 'CLEAR']
							},
							name: name,
			                model: { label: { type: 'text' } },
			                height: '400px',
			                data(){
			                    return treeData
			                }
			            }); 
			}
			
		}
		 exports("hsw", hsw);
		
});