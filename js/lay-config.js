/**
 * date:2019/08/16
 * author:Mr.Chung
 * description:此处放layui自定义扩展
 */

window.rootPath = (function (src) {
    src = document.scripts[document.scripts.length - 1].src;
    return src.substring(0, src.lastIndexOf("/") + 1);
})();

layui.config({
    base: rootPath + "lay-module/",
    version: true
}).extend({
    miniAdmin: "layuimini/miniAdmin", // layuimini后台扩展
    miniMenu: "layuimini/miniMenu", // layuimini菜单扩展
    miniPage: "layuimini/miniPage", // layuimini 单页扩展
    miniTheme: "layuimini/miniTheme", // layuimini 主题扩展
    miniTongji: "layuimini/miniTongji", // layuimini 统计扩展
    step: 'step-lay/step', // 分步表单扩展
    treeTable: 'treeTable/treeTable', //table树形扩展
    tableSelect: 'tableSelect/tableSelect', // table选择扩展
    iconPickerFa: 'iconPicker/iconPickerFa', // fa图标选择扩展
    echarts: 'echarts/echarts', // echarts图表扩展
    echartsTheme: 'echarts/echartsTheme', // echarts图表主题扩展
    wangEditor: 'wangEditor/wangEditor', // wangEditor富文本扩展
    layarea: 'layarea/layarea', //  省市县区三级联动下拉选择器
	hsw:'hsw/hsw'
});

//test
//var HSW_BASE_API = 'http://api.hsw666.cn/api';
//dev
var HSW_BASE_API = 'http://192.168.62.101:1022/api';
//dev
//var HSW_BASE_API = 'http://127.0.0.1:1022/api';
/* 用户相关接口 */
var HSW_API_MANAGE_USER_LIST = HSW_BASE_API + '/manage/user/list';
var HSW_API_MANAGE_USER_ADD = HSW_BASE_API + '/manage/user/add';
var HSW_API_MANAGE_USER_DEL = HSW_BASE_API + '/manage/user/del/pid';
var HSW_API_MANAGE_USER_EDIT = HSW_BASE_API + '/manage/user/edit/pid';
var HSW_API_MANAGE_USER_INFO = HSW_BASE_API + '/manage/user/info/pid';
var HSW_API_MANAGE_USER_RESET_PASSWORD = HSW_BASE_API + '/manage/user/reset/password/pid';

/* 角色相关 */
var HSW_API_MANAGE_ROLE_SELECT = HSW_BASE_API + '/manage/role/select';
var HSW_API_MANAGE_ROLE_LIST = HSW_BASE_API + '/manage/role/list';
var HSW_API_MANAGE_ROLE_ADD = HSW_BASE_API + '/manage/role/add';
var HSW_API_MANAGE_ROLE_EDIT = HSW_BASE_API + '/manage/role/edit/pid';
var HSW_API_MANAGE_ROLE_DEL = HSW_BASE_API + '/manage/role/del/pid';
var HSW_API_MANAGE_ROLE_INFO = HSW_BASE_API + '/manage/role/info/pid';

/* 权限相关 */
var HSW_API_MANAGE_PEIMISSION_TREE = HSW_BASE_API + '/manage/permission/tree';
var HSW_API_MANAGE_PEIMISSION_ADD = HSW_BASE_API + '/manage/permission/add';
var HSW_API_MANAGE_PEIMISSION_DEL = HSW_BASE_API + '/manage/permission/del/pid';
var HSW_API_MANAGE_PEIMISSION_INFO = HSW_BASE_API + '/manage/permission/info/pid';
var HSW_API_MANAGE_PEIMISSION_EDIT = HSW_BASE_API + '/manage/permission/edit/pid';
var HSW_API_MANAGE_PEIMISSION_EDIT_STATE = HSW_BASE_API+'/manage/permission/edit/state/pid';
var HSW_API_MANAGE_PEIMISSION_MENU = HSW_BASE_API+'/manage/permission/menu';

/* 角色权限 */
var HSW_API_MANAGE_ROLE_PERMISSION_INFO = HSW_BASE_API + '/manage/role/auth/pid';
var HSW_API_MANAGE_ROLE_AUTHORIZATION = HSW_BASE_API + '/manage/role/authorization';

/* 登录 */
var HSW_API_LOGIN_PASSWORD = HSW_BASE_API + '/manage/login/password';
